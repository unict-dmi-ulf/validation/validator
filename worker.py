from validation.course_access import CourseAccessValidator
from validation.token import TokenValidator
from validation import IValidator 
from flask import Flask
from flask import request

from retrieval import DataRetriever
from errors import ValidationException

app=Flask(__name__)

retriever = DataRetriever("http://localhost:5000/")
validators = [
    TokenValidator(),
    CourseAccessValidator()
]

def validateFeedbackRequest(retriever: DataRetriever, validators: 'list[IValidator]'):
    try:
        for validator in validators:
            validator.validate(retriever)
    except ValidationException as exception:
        return exception.get_body(), exception.get_status_code()

@app.route("/validation/addFeedback", methods=['POST'])
def addFeedback():
    retriever.setRequestData(request.get_json())

    return validateFeedbackRequest(retriever, validators)
