import requests

class DataRetriever:
    def __init__(self, address):
        self.__address = address
        self.__request_data = None

    def setRequestData(self, request_data):
        self.__request_data = request_data

    def retrieveTokenData(self):
        tokenId = self.__request_data['token']
        url_token = f"{self.__address}tokens/{tokenId}"
        response_token = requests.get(url_token)
        return response_token

    def retrieveLectureData(self):
        lectureId = self.__request_data['lecture']
        url_lecture = f"{self.__address}lectures/{lectureId}"
        response_lecture = requests.get(url_lecture)
        return response_lecture
