class ValidationException(Exception):
    def __init__(self, body, status_code):
        self.__body = body
        self.__status_code = status_code

    def get_body(self):
        return self.__body

    def get_status_code(self):
        return self.__status_code
