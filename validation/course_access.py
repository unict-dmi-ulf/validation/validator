from validation import IValidator
from errors import *

class CourseAccessValidator(IValidator):
    def validate(self, retriever):
        token_data = retriever.retrieveTokenData()
        lecture_data = retriever.retrieveLectureData()

        if lecture_data["course"] not in token_data["accessibleCourses"]:
            raise ValidationException("Internal server error", 500)
