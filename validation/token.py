from validation import IValidator
from errors import *

class TokenValidator(IValidator):
    def validate(self, retriever):
        token_data = retriever.retrieveTokenData()

        if token_data.status_code == 404:
            raise ValidationException("Internal server error", 500)
