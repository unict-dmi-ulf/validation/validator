from abc import abstractmethod
from retrieval import DataRetriever

class IValidator:
    @abstractmethod
    def validate(self, retriever: DataRetriever): 
        pass
